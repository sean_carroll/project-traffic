from gitlab.v4.objects import Project, MergeRequest
from typing import List
from datetime import datetime, timedelta
import pandas as pd

def get_mrs_by_project(project: Project) -> List[MergeRequest]:
    start_date = datetime.today() - timedelta(days=365)
    end_date = datetime.today()
    mrs = project.mergerequests.list(get_all=True, created_after=start_date, created_before=end_date, scope="all")
    rows = []
    for mr in mrs:
        merged_by_name = ""
        merged_by_username = ""
        if mr.merged_by:
            merged_by_name = mr.merged_by["name"]
            merged_by_username = mr.merged_by["username"]
        rows.append([mr.title, mr.created_at, mr.merged_at, merged_by_name, merged_by_username, mr.web_url])
    
    mr_df = pd.DataFrame(rows, columns=["TITLE", "CREATED", "MERGED", "MERGED_BY", "MERGED_BY_USERNAME", "URL"])
    mr_df["CREATED"] = pd.to_datetime(mr_df["CREATED"])
    mr_df = mr_df.sort_values(by="CREATED")
    mr_df['CREATED'] = pd.to_datetime(mr_df['CREATED'])
    mr_df['MERGED'] = pd.to_datetime(mr_df['MERGED'])
    mr_df['MONTH_YEAR'] = mr_df['CREATED'].dt.to_period('M').dt.strftime('%Y-%m')
    return mr_df

def get_active_maintainers(mrs_df: pd.DataFrame) -> pd.DataFrame:
    filtered_df = mrs_df[mrs_df['MERGED_BY'] != '']
    maintainers_df = filtered_df[['MERGED_BY', 'MERGED_BY_USERNAME']].drop_duplicates().reset_index(drop=True)
    maintainers_df = maintainers_df.rename(columns={'MERGED_BY': 'Name', 'MERGED_BY_USERNAME': "Username"})
    return maintainers_df
