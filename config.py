from yaml import safe_load
from streamlit import cache_data

@cache_data(ttl=360)
def load_config():
    with open("./config.yaml") as f:
        return safe_load(f.read())