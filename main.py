import streamlit as st
from mrs import get_mrs_by_project, get_active_maintainers
from project import get_project_by_name, get_maintainers
from charts.monthly_mrs import show_monthly_mrs, show_monthly_mrs_by_maintainer, show_min_and_max_mrs_per_maintainer
from config import load_config
from team_members import get_maintainers_from_team_members

project_name = ''
submitted = False

def main():
    st.set_page_config(
        page_title="Project Traffic",
        page_icon=":chart_with_upwards_trend:",
        layout="wide",
    )

    config = load_config()

    with st.sidebar:
        st.title('Project Traffic')
        st.subheader('Get traffic stats for a GitLab Project')
        with st.form("my_form"):
            # project_name = st.text_input(label="Project", help="e.g. gitlab-org/editor-extensions/gitlab-lsp")
            selected_project = st.selectbox("Project", options=config['projects'].keys())
            submitted = st.form_submit_button("Search", type='primary')

    if submitted and selected_project:
        project_name = config['projects'][selected_project]
        project = get_project_by_name(project_name)
        # maintainers = {"1", "2", "3", "4"}
        # maintainers = get_maintainers(project)
        maintainers = None

        mrs = get_mrs_by_project(project)
        if project_name in config['mapping']:
            mapping_project_name = config['mapping'][project_name]
            maintainers = get_maintainers_from_team_members(mapping_project_name)
        else:
            maintainers = get_active_maintainers(mrs)
        no_of_maintainers = len(maintainers)
    
        col1, col2, col3 = st.columns(3)

        with col1:
            st.metric("Project ID", project.id)
            st.metric("Total MRs in the year", len(mrs))

        with col2:
            st.metric("Project Name", project.name)
            
        with col3:
            st.metric("Active Maintainers", no_of_maintainers, help="Number of maintainers who have merged at least 1 MR")

        show_min_and_max_mrs_per_maintainer(mrs, no_of_maintainers, col2, col3)
        show_monthly_mrs(mrs)
        show_monthly_mrs_by_maintainer(mrs, no_of_maintainers)
        st.subheader("All MRs")
        st.dataframe(mrs, use_container_width=True)
        st.subheader("Active Maintainer List")
        st.dataframe(maintainers)

if __name__ == '__main__':
    main()