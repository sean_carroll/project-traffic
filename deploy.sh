kubectl delete secret project-traffic-secret
kubectl create secret generic project-traffic-secret \
    --from-literal=GITLAB_TOKEN=$GITLAB_TOKEN \
    --from-file=$(pwd)/secrets/oauth2-proxy.cfg

kubectl apply -f k8s/