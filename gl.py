import gitlab
import os

def get_gl() -> gitlab.Gitlab:
    GITLAB_TOKEN = os.getenv("GITLAB_TOKEN")
    gl = gitlab.Gitlab(url='https://gitlab.com', private_token=GITLAB_TOKEN) 
    return gl
